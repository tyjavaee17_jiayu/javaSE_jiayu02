package com.dao;

import com.entity.Province;

import java.util.ArrayList;
import java.util.List;

public class ProvinceDao {
    private static List<Province> provinceList = new ArrayList<>();
    static {
        provinceList.add(new Province(1,"广东"));
        provinceList.add(new Province(2,"辽宁"));
        provinceList.add(new Province(3,"湖南"));
        provinceList.add(new Province(4,"河北"));
    }
    public List<Province> findAll(){
        return provinceList;
    }
}
