package com.web.servlet;

import com.entity.Province;
import com.service.ProvinceService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ProvinceListServlet", urlPatterns = "/ProvinceListServlet")
public class ProvinceListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        List<Province> provinces = new ProvinceService().findAll();
        request.setAttribute("provinces",provinces);
        request.getRequestDispatcher("/List.jsp").forward(request,response);
    }
}
