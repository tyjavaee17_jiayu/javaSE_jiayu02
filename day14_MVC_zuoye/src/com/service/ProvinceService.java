package com.service;

import com.dao.ProvinceDao;
import com.entity.Province;

import java.util.List;

public class ProvinceService {
    //实例dao类
    private ProvinceDao provinceDao = new ProvinceDao();
    //获取省份列表数据
    public List<Province> findAll(){
        return provinceDao.findAll();
    }
}
