<%--
  Created by IntelliJ IDEA.
  User: jiayu
  Date: 2020/5/6
  Time: 23:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h3>省份列表</h3>
<select>
    <option>请选择</option>
    <c:forEach items="${provinces}" var="province">
        <option value="${province.id}">${province.provinceName}</option>
    </c:forEach>
</select>
</body>
</html>
