package com.servlet.Demo01ServletContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@WebServlet(name = "Demo02Servlet", urlPatterns = "/Demo02Servlet")
public class Demo02Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        InputStream resourceAsStream = getServletContext().getResourceAsStream("img/3.jpg");
        int len = 0;
        byte[] bytes = new byte[1024];
        while ((len = resourceAsStream.read(bytes))!=-1){
            response.getOutputStream().write(bytes,0,len);
        }
        resourceAsStream.close();
    }
}
