package com.servlet.Demo02servletcontext_读取全局参数;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet(name = "Demo03Servlet", urlPatterns = "/Demo03Servlet")
public class Demo03Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Enumeration<String> initParameterNames = getServletContext().getInitParameterNames();
        while(initParameterNames.hasMoreElements()){
            String paremName = initParameterNames.nextElement();
            String paremValue = getServletContext().getInitParameter(paremName);
            System.out.println(paremName + paremValue);
        }
    }
}
