package com.servlet.DownloadServlet;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;

@WebServlet(name = "DownloadServlet", urlPatterns = "/DownloadServlet")
public class DownloadServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        /*1.获取要下载的文件的名字*/
        String fn = request.getParameter("fn");
        /*2.告诉浏览器返回的文件的类型，并且要下载该文件*/
        response.setHeader("Content-Type",super.getServletContext().getMimeType(fn));
        response.setHeader("Content-Disposition","attachment;filename = "+fn);
        /*3.读取服务器中的资源，传输给浏览器*/
        //服务器中得数据得路径必须是绝对路径
        IOUtils.copy(new FileInputStream("E:\\IdeaProject\\WEB\\day09_Servlet_ServletContext\\web\\img\\"+fn),response.getOutputStream());

    }
}
