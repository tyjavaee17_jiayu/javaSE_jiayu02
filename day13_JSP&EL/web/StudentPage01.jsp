<%@ page import="com.student.Student" %><%--
  Created by IntelliJ IDEA.
  User: jiayu
  Date: 2020/5/5
  Time: 21:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/Css.css">
</head>
<body>
<%
    request.setCharacterEncoding("utf-8");
    response.setContentType("text/html;charset=utf-8");
    Student student = new Student("贾宇", 26, "太原", 102888888, 1565555454);
    request.setAttribute("jiayu",student);
%>
    <h1>学生信息</h1>
    <h4>姓名；${jiayu.name}</h4>
    <h4>年龄；${jiayu.age}</h4>
    <h4>地址；${jiayu.address}</h4>
    <h4>QQ;${jiayu.qq}</h4>
    <h4>电话；${jiayu.phoneNumber}</h4>
</body>
</html>
