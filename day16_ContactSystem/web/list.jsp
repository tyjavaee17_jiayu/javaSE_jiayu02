<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<!-- 网页使用的语言 -->
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>查询所有联系人</title>

    <!-- 1. 导入CSS的全局样式 -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- 2. jQuery导入，建议使用1.9以上的版本 -->
    <script src="js/jquery-2.1.0.min.js"></script>
    <!-- 3. 导入bootstrap的js文件 -->
    <script src="js/bootstrap.min.js"></script>
        <style type="text/css">
        tr,th {
            text-align: center;
        }
        .col-md-4{
            padding-right:0px;
            padding-left:0px;
        }
    </style>
</head>
<body>
<div class="container">
    <h3 align="center">联系人管理</h3><br>
   
    <form action="${pageContext.request.contextPath}/ContactListServlet" method="get" class="form-inline" id="contactForm">

		<div class="row text-right" style="margin-bottom: 10px; margin-top: 15px;">
            <div class="col-md-4 text-left">
                <a class="btn btn-primary" style="width: 120px" href="add.jsp">添加联系人</a>
            </div>
        </div>
        <div class="row">
            <table border="1" class="table table-bordered table-hover">
                <tr class="success">
                    <th>编号</th>
                    <th>姓名</th>
                    <th>性别</th>
                    <th>年龄</th>
                    <th>籍贯</th>
                    <th>QQ</th>
                    <th>邮箱</th>
                    <th>操作</th>
                </tr>
				<c:forEach items="${contactList}" var="contact">
                    <tr>
					<td>${contact.id}</td>
					<td>${contact.name}</td>
					<td>${contact.sex}</td>
					<td>${contact.age}</td>
					<td>${contact.address}</td>
					<td>${contact.qq}</td>
					<td>${contact.email}</td>
					<td>
						<div class="btn-group btn-group-sm">
							<a class="btn btn-success btn-xs" href="${pageContext.request.contextPath}/FindContactServlet?id=${contact.id}">修改</a>&nbsp;
							<a class="btn btn-info btn-xs" href="javascript:onDeletContact(${contact.id})">删除</a>
						</div>
					</td>
				</tr>
                </c:forEach>
            </table>
        </div>
    </form>
</div>
</body>
<script type="text/javascript">
    function onDeletContact(id) {
        if (confirm("您确定要删除当前数据嘛？")){
            location.href="${pageContext.request.contextPath}/DeletContactServlet?id="+id;
        }
    }
</script>

</html>
