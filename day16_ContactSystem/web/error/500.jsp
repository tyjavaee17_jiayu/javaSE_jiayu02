<%@ page contentType="text/html;charset=UTF-8" language="java" isErrorPage="true" %>
<%--
    当前错误友好页面的职责：打印异常信息的步骤
        1.在page指令使用isErrorPage=true 代表接收传过来的异常信息exception对象
          isErrorPage这个属性默认为false,需要手动修改为true
        2.在当前页面使用java代码打印异常信息(只有isErrorPage=true，脚本才可以使用exception对象)
--%>
<%
    exception.printStackTrace();//打印控制台是临时信息，以后会输出到日志文件中
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h2>服务器忙，请明天再来访问。。。</h2>
<a href="../index.jsp">返回首页</a>
</body>
</html>
