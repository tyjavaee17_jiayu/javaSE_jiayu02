<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
404不是错误，不可以使用exception对象，如果使用这个对象也是null
--%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h2>您访问的资源走丢了。</h2>
<a href="../index.jsp">返回首页</a>
</body>
</html>
