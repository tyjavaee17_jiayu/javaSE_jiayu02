package com.web.servlet;

import com.entity.Contact;
import com.service.ContactService;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@WebServlet(name = "AddContactServlet", urlPatterns = "/AddContactServlet")
public class AddContactServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
    private ContactService contactService = new ContactService();
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Contact contact = new Contact();
            Map<String, String[]> parameterMap = request.getParameterMap();
            BeanUtils.populate(contact,parameterMap);
            contactService.addContact(contact);

            response.sendRedirect(request.getContextPath()+"/ContactListServlet");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
