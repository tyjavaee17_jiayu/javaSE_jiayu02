package com.web.servlet;

import com.entity.Contact;
import com.service.ContactService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ContactListServlet", urlPatterns = "/ContactListServlet")
public class ContactListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Contact> contactList = new ContactService().findAll();
        request.setAttribute("contactList",contactList);
        request.getRequestDispatcher("/list.jsp").forward(request,response);
    }
}
