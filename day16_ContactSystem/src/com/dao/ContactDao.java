package com.dao;

import com.entity.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactDao {
    private static List<Contact> contactList = new ArrayList<>();
    private static Integer index = 0;
    static {
        contactList.add(new Contact(++index,"张三","男",18,"山西","155555555","11111@qq.com"));
        contactList.add(new Contact(++index,"李四","女",19,"河北","166666666","22222@qq.com"));
        contactList.add(new Contact(++index,"王五","男",20,"江苏","177777777","33333@qq.com"));
        contactList.add(new Contact(++index,"赵六","男",21,"湖南","188888888","44444@qq.com"));
        contactList.add(new Contact(++index,"田七","女",22,"广西","199999999","55555@qq.com"));
    }
    public List<Contact> findAll(){
        return contactList;
    }
    public void add(Contact  contact){
        contact.setId(++index);
        contactList.add(contact);
    }
    public void update(Contact contact){
        if (contactList.size()>0){
            for (int i = 0; i < contactList.size(); i++) {
                if (contactList.get(i).getId().equals(contact.getId())){
                    contactList.set(i,contact);
                    return;
                }
            }
        }
    }
    public Contact findById(Integer id){
        if(contactList.size()>0){
            for (Contact contact : contactList) {
                if (contact.getId().equals(id)){
                    return contact;
                }
            }
        }
        return null;
    }
    public void delet(Integer id){
        if (contactList.size()>0){
            for (Contact contact : contactList) {
                if (contact.getId().equals(id)){
                    contactList.remove(contact);
                    return;
                }
            }
        }
    }
}
