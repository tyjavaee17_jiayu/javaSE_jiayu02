package com.service;

import com.dao.ContactDao;
import com.entity.Contact;

import java.util.List;

public class ContactService {
    private ContactDao contactDao = new ContactDao();
    public List<Contact> findAll(){
        return contactDao.findAll();
    }
    public void addContact(Contact contact){
        contactDao.add(contact);
    }
    public void updateContact(Contact contact){
        contactDao.update(contact);
    }
    public Contact findById(Integer id){
        return contactDao.findById(id);
    }
    public void deletContact(Integer id){
        contactDao.delet(id);
    }
}
