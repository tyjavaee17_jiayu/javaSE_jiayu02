package Demo01_request_请求行.com.ithiema.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DemoServlet", urlPatterns = "/DemoServlet")
public class DemoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String method = request.getMethod();
        System.out.println("method：" + method);

        //获取url?get提交的数据（带协议的绝对路径）
        String Url = request.getRequestURL().toString();
        System.out.println("Url："+Url);

        //获取协议版本
        String protocol = request.getProtocol();
        System.out.println("当前协议版本："+protocol);

        String clientIp = request.getRemoteAddr();
        System.out.println("当前客户端得IP："+clientIp);


        //获取当前项目部署资源目录名字（虚拟路径）【重要】
        String contextPath = request.getContextPath();
        System.out.println("当前项目部署资源目录名字:"+contextPath);


        //获取当前资源路径
        String servletPath = request.getServletPath();
        System.out.println("当前资源路径:" + servletPath);

        //获取uri（不带协议的绝对路径）
        String URI = request.getRequestURI();
        System.out.println("URI："+URI);

    }
}
