package Demo02_request_请求头.com.itheima.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UserAgentServlet", urlPatterns = "/UserAgentServlet")
public class UserAgentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userAgent = request.getHeader("user-agent");
        System.out.println(userAgent);



        /*
         * 输出的格式：Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
         *
         * 格式介绍【了解】
         *  Mozilla/5.0： 含义是火狐基金会
         *  (Windows NT 10.0; Win64; x64)： 客户端操作系统的说明，如果是IE浏览器，内核信息会在这里
         * AppleWebKit/537.36 (KHTML, like Gecko)： 浏览器的内核
         *  Chrome/79.0.3945.130 Safari/537.36 ：浏览器版本的信息
         * */
    }
}
