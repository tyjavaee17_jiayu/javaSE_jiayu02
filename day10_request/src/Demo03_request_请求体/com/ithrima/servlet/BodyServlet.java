package Demo03_request_请求体.com.ithrima.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author 黑马程序员
 */
@WebServlet(name = "BodyServlet", urlPatterns = "/BodyServlet")
public class BodyServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //获取用户名
        String name = request.getParameter("name");

        //获取性别（单选框只会提交一个选中的value）
        String gender = request.getParameter("gender");

        //获取城市(单选下拉列表只会提交一个选中value)
        String city = request.getParameter("city");

        //获取爱好(复选框会提交所有选中的value,返回一个数组)
        String[] hobbies = request.getParameterValues("hobby");

        //打印数据
        System.out.println("接收到请求数据如下：");
        System.out.println("用户名："+name);
        System.out.println("性别："+gender);
        System.out.println("城市："+city);
        System.out.println("爱好："+ Arrays.toString(hobbies));
        //Arrays.toString(hobbies) ,可以将数组转换为字符串输出，元素之间使用,隔开

    }
}