package Demo03_request_请求体.com.ithrima.servlet;

import java.util.Arrays;

public class User {
    private String name;
    private String gender;
    private String city;
    private String[] hobby;

    public User() {
    }

    public User(String name, String gender, String city, String[] hobby) {
        this.name = name;
        this.gender = gender;
        this.city = city;
        this.hobby = hobby;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String[] gethobby() {
        return hobby;
    }

    public void sethobby(String[] hobby) {
        this.hobby = hobby;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", city='" + city + '\'' +
                ", hobby=" + Arrays.toString(hobby) +
                '}';
    }
}
