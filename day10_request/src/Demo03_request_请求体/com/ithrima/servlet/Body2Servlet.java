package Demo03_request_请求体.com.ithrima.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author 黑马程序员
 */
@WebServlet(name = "Body2Servlet", urlPatterns = "/Body2Servlet")
public class Body2Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //读取post数据之前，设置request默认码表为utf8
        request.setCharacterEncoding("utf8");


        String name = request.getParameter("name");


        String gender = request.getParameter("gender");


        String city = request.getParameter("city");


        String[] hobbies = request.getParameterValues("hobby");

        //打印数据
        System.out.println("接收到请求数据如下：");
        System.out.println("用户名："+name);
        System.out.println("性别："+gender);
        System.out.println("城市："+city);
        System.out.println("爱好："+ Arrays.toString(hobbies));
    }
}