<%--
  Created by IntelliJ IDEA.
  User: jiayu
  Date: 2020/5/6
  Time: 22:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
</head>
<body>
    <table class="table table-bordered table-hover" style="width:500px">
        <tr class="active">
            <td>属性</td>
            <td>内存</td>
            <td>JVM</td>
        </tr>
        <c:forEach items="${infos}" var="info">
            <tr>
                <td>${info.property}</td>
                <td>${info.memory}</td>
                <td>${info.jvm}</td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
