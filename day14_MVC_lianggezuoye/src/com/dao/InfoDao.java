package com.dao;

import com.entity.Info;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TreeSet;

public class InfoDao {
    private static TreeSet<Info> infos = new TreeSet<>();
    static {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(InfoDao.class.getClassLoader().getResourceAsStream("info.txt"),"utf-8"));
            String line;
            while ((line=bufferedReader.readLine())!=null){
                String[] split = line.split(" ");
                Info info = new Info(Integer.parseInt(split[0]), split[1], split[2], split[3]);
                infos.add(info);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public TreeSet<Info> findAll(){
        return infos;
    }
}
