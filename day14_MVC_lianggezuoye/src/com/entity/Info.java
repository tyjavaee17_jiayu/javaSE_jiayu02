package com.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Info implements Comparable<Info>{
    private Integer id;
    private String property;
    private String memory;
    private String jvm;

    @Override
    public int compareTo(Info o) {
        return this.id=o.id;
    }
}
