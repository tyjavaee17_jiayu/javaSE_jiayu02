package com.service;

import com.dao.InfoDao;
import com.entity.Info;

import java.util.TreeSet;

public class InfoService {
    public TreeSet<Info> findAll(){
        return new InfoDao().findAll();
    }
}
