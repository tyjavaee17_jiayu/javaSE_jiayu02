package com.itheima.service;

import com.itheima.dao.ContactDao;
import com.itheima.entity.Contact;

import java.util.List;

public class ContactService {
    private ContactDao contactDao = new ContactDao();
    public List<Contact> findAll(){
        return contactDao.findAll();
    }
}
