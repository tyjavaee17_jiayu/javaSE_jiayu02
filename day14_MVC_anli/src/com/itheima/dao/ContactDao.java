package com.itheima.dao;

import com.itheima.entity.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactDao {
    private static List<Contact> contactList;
    static {
        contactList = new ArrayList<>();
        contactList.add(new Contact(1,"猪八戒","男",25,"广东","1234567","podijgrowg@qq.com"));
        contactList.add(new Contact(2,"貂蝉","女",25,"辽宁","1234567","podijgrowg@qq.com"));
        contactList.add(new Contact(3,"孙悟空","男",25,"北京","1234567","podijgrowg@qq.com"));
        contactList.add(new Contact(4,"周瑜","男",25,"上海","1234567","podijgrowg@qq.com"));
    }
    public List<Contact> findAll(){
        return contactList;
    }
}
