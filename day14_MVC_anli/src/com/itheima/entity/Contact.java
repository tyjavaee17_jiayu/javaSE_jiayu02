package com.itheima.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Contact {
    private Integer id; //编号，推荐使用包装类型，可以赋值为null
    private String name;//姓名
    private String sex;  //性别
    private Integer age; //年龄
    private String address; //籍贯
    private String qq;      //qq
    private String email;   //邮箱
}
