package com.itheima.web.servlet;

import com.itheima.entity.Contact;
import com.itheima.service.ContactService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ListContactServlet", urlPatterns = "/ListContactServlet")
public class ListContactServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
    private ContactService contactService = new ContactService();
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Contact> contactList = contactService.findAll();
        request.setAttribute("contactList",contactList);
        request.getRequestDispatcher("/list.jsp").forward(request,response);
    }
}
