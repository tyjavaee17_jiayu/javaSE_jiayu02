package com.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageBean<T> {
    private List<T> dataList;
    private int firstPage;
    private int prePage;
    private int nextPage;
    private int totalPage;
    private int curPage;
    private int count;
    private int pageSize;

    public static <T> PageBean<T> getPageBean(int curPage,int pageSize,int count,List<T> dataList){
        PageBean pageBean = new PageBean();
        pageBean.setDataList(dataList);
        pageBean.setCurPage(curPage);
        pageBean.setPrePage(curPage-1);
        pageBean.setNextPage(curPage+1);
        pageBean.setFirstPage(1);
        pageBean.setPageSize(pageSize);
        pageBean.setCount(count);
        int totalPage = count%pageSize==0?count/pageSize:count/pageSize+1;
        pageBean.setTotalPage(totalPage);
        return pageBean;

    }
}
