package com.service;

import com.dao.StudentDao;
import com.entity.PageBean;
import com.entity.Student;

import java.util.List;

public class StudentService {
    /*
     * 根据第几页和每页查询几条数据封装PageBean
     * @param curPage
     * @param pageSize
     * @return
     */
    public PageBean<Student> findByPage(int curPage, int pageSize){
        List<Student> students = new StudentDao().findByPage((curPage - 1) * pageSize, pageSize);
        long count = new StudentDao().count();
        PageBean<Student> pageBean = PageBean.getPageBean(curPage, pageSize, (int) count, students);
        return pageBean;
    }

}
