package com.dao;

import com.entity.Student;
import com.web.listener.ContextLoaderListener;

import java.util.List;
import java.util.stream.Collectors;

public class StudentDao {
    /*
     * start表示开始查询的索引，从0开始
     * count表示要查询几条数据
     * @param startIndex
     * @param count
     * @return
     */
    public List<Student> findByPage(int startIndex, int count){
        List<Student> students = ContextLoaderListener.students.stream().skip(startIndex).limit(count).collect(Collectors.toList());
        return students;
    }
    public long count(){
        return ContextLoaderListener.students.stream().count();
    }
}
