package com.web.listener;

import com.entity.Student;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ContextLoaderListener implements ServletContextListener {
    public static List<Student> students = new ArrayList<>();
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String configLocation = servletContextEvent.getServletContext().getInitParameter("contextConfigLocation");

        if (configLocation.startsWith("classpath;")){
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(ContextLoaderListener.class.getClassLoader().getResourceAsStream(configLocation.substring(10)), "utf-8"));
                String line;
                while ((line=bufferedReader.readLine())!=null){
                    String[] str = line.split(" ");
                    new Student(str[0],str[1],Integer.parseInt(str[2]),str[3]);
                    bufferedReader.close();
                    System.out.println(students);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
