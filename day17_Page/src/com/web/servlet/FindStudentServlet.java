package com.web.servlet;

import com.entity.PageBean;
import com.entity.Student;
import com.service.StudentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "FindStudentServlet", urlPatterns = "/FindStudentServlet")
public class FindStudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String curPage = request.getParameter("curPage");
        String pageSize = request.getParameter("pageSize");

        int curPage1 = (curPage==""||curPage==null)?1:Integer.parseInt(curPage);
        int pageSize1 = (pageSize==""||pageSize==null)?1:Integer.parseInt(pageSize);

        PageBean<Student> pageBean = new StudentService().findByPage(curPage1,pageSize1);

        request.setAttribute("pageBean",pageBean);
        request.getRequestDispatcher("/list.jsp").forward(request,response);
    }
}
