<%--
  Created by IntelliJ IDEA.
  User: jiayu
  Date: 2020/5/11
  Time: 11:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
</head>
<body>
    <div>
        <table style="width: 600px" class="table table-hover table table-bordered table table-striped">
            <tr class="success">
                <td>#</td>
                <td>学号</td>
                <td>姓名</td>
                <td>年龄</td>
                <td>地址</td>
            </tr>
            <c:forEach items="${pageBean.dataList}" var="stu" varStatus="vs">
                <td>${vs.count}</td>
                <td>${stu.stuID}</td>
                <td>${stu.name}</td>
                <td>${stu.age}</td>
                <td>${stu.address}</td>
            </c:forEach>
        </table>
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li class="disabled">
                    <a href="${pageContext.request.contextPath}/FindStudentServlet?curPage=${pageBean.curPage-1}&pageSize=2" aria-label="Previous">
                        <span aria-hidden="true">«</span>
                    </a>
                </li>
                <c:forEach begin="1" end="${pageBean.totalPage}" step="1" var="num">
                <li ${pageBean==num?'class="active"':''}>
                    <a href="${pageContext.request.contextPath}/FindStudentServlet?curPage=${num}&pageSize=2#">${num}
                    </a>
                </li>
                </c:forEach>
                <li>
                    <a href="${pageContext.request.contextPath}/FindStudentServlet?curPage=${pageBean.curPage+1}&pageSize=2" aria-label="Next">
                        <span aria-hidden="true">»</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</body>
</html>
