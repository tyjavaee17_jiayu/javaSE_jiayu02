package com.itheima.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletA", urlPatterns = "/ServletA")
public class ServletA extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //解决response输出中文乱码
        response.setContentType("text/html;charset=utf8");

        //1.获取上一个资源地址
        String referer = request.getHeader("referer");

        //2.判断资源地址是不是自己download.html下载页面来的请求
        //获取部署目录名字的语法：request.getContextPath()="/资源部署目录名字"
        if(referer!=null && referer.equals("http://localhost:8080"+request.getContextPath()+"/download.html")) {
            //2.1 是，允许下载
            response.getWriter().print("下载成功");
        }else {
            //2.2 不是，不允许下载
            response.getWriter().print("小偷，再偷我就报警了");
        }
    }
}
