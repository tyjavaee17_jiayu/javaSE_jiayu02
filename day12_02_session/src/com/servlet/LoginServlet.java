package com.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        String sessionCheckCode = (String) request.getSession().getAttribute("code_session");
        String userCheckcode = request.getParameter("checkcode");
        if (!sessionCheckCode.equalsIgnoreCase(userCheckcode)){
            response.getWriter().print("<script>alert('验证码错误');history.back();</script>");
            return;
        }
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (username.equals("Jack")&&password.equals("123")){
            request.getSession().setAttribute("loginUser",username);
            response.sendRedirect(request.getContextPath()+"/WelcomeServlet");
        }else {
            response.getWriter().print("登录失败！");
        }
    }
}
