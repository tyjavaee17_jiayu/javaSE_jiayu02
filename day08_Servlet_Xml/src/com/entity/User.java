package com.entity;

public class User {
    private int ID;
    private String name;
    private int Age;

    public User() {
    }

    public User(int ID, String name, int age) {
        this.ID = ID;
        this.name = name;
        Age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", Age=" + Age +
                '}';
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }
}
