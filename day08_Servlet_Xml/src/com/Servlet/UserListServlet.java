package com.Servlet;

import com.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "UserListServlet", urlPatterns = "/UserListServlet")
public class UserListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");

        List<User> userList = new ArrayList<>();
        userList.add(new User(100,"张三",20));
        userList.add(new User(200,"李四",22));
        userList.add(new User(300,"王五",18));
        userList.add(new User(400,"赵六",44));
        userList.add(new User(500,"王八",24));


        PrintWriter pw = response.getWriter();
        pw.write("<table align='center' border='1px' cellpadding='5px' cellspacing='0' width='30%'>");

        pw.write("<tr>");
        pw.write("<th>编号</th>");
        pw.write("<th>姓名</th>");
        pw.write("<th>年龄</th>");
        pw.write("</tr>");

        for (User user : userList) {
            pw.write("<tr>");
            pw.write("<td>"+user.getID()+"</td>");
            pw.write("<td>"+user.getName()+"</td>");
            pw.write("<td>"+user.getAge()+"</td>");
            pw.write("</tr>");
        }

        pw.write("</table>");
    }
}
