package com.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieUtils {
    public static Cookie getCookieByName(String cookieName , HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        if (cookies!= null){
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName)){
                    return cookie;
                }
            }
        }
        return null;
    }
}
