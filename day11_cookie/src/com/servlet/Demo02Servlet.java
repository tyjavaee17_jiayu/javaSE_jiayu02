package com.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Demo02Servlet", urlPatterns = "/Demo02Servlet")
public class Demo02Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String remember = request.getParameter("remember");
        if (username.equals("jack")&&password.equals("123")){
            response.getWriter().print("登录成功，欢迎"+username);
            if (remember.equalsIgnoreCase("on")){
                Cookie userNameCookie = new Cookie("username", username);
                userNameCookie.setMaxAge(60*60*24*7);
                userNameCookie.setPath("/");
                response.addCookie(userNameCookie);

                Cookie passWordCookie = new Cookie("passWord", password);
                passWordCookie.setMaxAge(60*60*24*7);
                passWordCookie.setPath("/");
                response.addCookie(passWordCookie);
            }
        }else{
            response.sendRedirect("failure.html");
        }
    }
}
