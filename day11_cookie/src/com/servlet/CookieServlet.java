package com.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CookieServlet", urlPatterns = "/CookieServlet")
public class CookieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie cookie = new Cookie("username", "jiayu");
        cookie.setMaxAge(60*60);
        cookie.setPath("/");
        response.addCookie(cookie);
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie1 : cookies) {
            String key = cookie1.getName();
            System.out.println(key +"="+ cookie1.getValue());
        }
    }
}
