package com.web.listener;

import com.oracle.webservices.internal.api.message.PropertySet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ContextLoaderListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        //获取web.xml中contextConfigLocation的值
        String location = servletContextEvent.getServletContext().getInitParameter("contextConfigLocation");

        //解析contextConfigLocation的值，读取配置文件信息
        if (location.startsWith("classpath")){
            try {
                InputStream resourceAsStream = ContextLoaderListener.class.getClassLoader().getResourceAsStream(location.substring(10));
                Properties properties = new Properties();
                properties.load(resourceAsStream);
                System.out.println("加载配置文件完毕，配置文件中得信息："+properties);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
