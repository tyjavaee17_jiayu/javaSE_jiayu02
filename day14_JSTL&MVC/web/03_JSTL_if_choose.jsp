<%--
  Created by IntelliJ IDEA.
  User: jiayu
  Date: 2020/5/6
  Time: 20:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>\
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    session.setAttribute("loginUser","admin");
%>
    <c:if test="${empty loginUser}">
        <p>请登录</p>
    </c:if>
    <c:if test="${not empty loginUser}">
        <p>欢迎${loginUser}登录成功！</p>
    </c:if>
</body>
</html>
