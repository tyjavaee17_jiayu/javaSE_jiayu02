<%@ page import="java.util.ArrayList" %>
<%@ page import="com.user.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: jiayu
  Date: 2020/5/6
  Time: 20:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <c:forEach begin="1" end="10" step="1">
        <p>我爱java</p>
    </c:forEach>

    <%
        List<User> users = new ArrayList<>();
        users.add(new User("张三",15));
        users.add(new User("李四",18));
        users.add(new User("王五",16));
        pageContext.setAttribute("userList",users);
    %>
<c:forEach items="${userList}" var="user">
    <p>姓名：${user.name},年龄：${user.age}</p>
</c:forEach>

<%
    HashMap<String, User> userMap = new HashMap<>();
    userMap.put("laoda",new User("张三",15));
    userMap.put("laoer",new User("李四",16));
    userMap.put("laosan",new User("王五",17));
    pageContext.setAttribute("userMap",userMap);
%>
<c:forEach items="${userMap}" var="entry">
    <p>${entry.key},用户的名字：${entry.value.name},用户的年龄：${entry.value.age}</p>
</c:forEach>
</body>
</html>
