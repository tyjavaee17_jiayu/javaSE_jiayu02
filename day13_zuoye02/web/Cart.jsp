<%--
  Created by IntelliJ IDEA.
  User: jiayu
  Date: 2020/5/6
  Time: 19:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
</head>
<body>
    <h1>购物车信息</h1>
    <table class="table table-striped table-bordered ">
        <tr>
            <th>编号</th>
            <th>商品名称</th>
            <th>商品数量</th>
        </tr>
        <c:forEach items="${cart}" var="entry" varStatus="vx">
            <tr>
                <th scope="row">${vx.count}</th>
                <td>${entry.key}</td>
                <td>${entry.value}</td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
