<%--
  Created by IntelliJ IDEA.
  User: jiayu
  Date: 2020/5/6
  Time: 18:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
</head>
<body>
    <h1 style="color: red;padding: 1px;">商品信息</h1>
    <table class="table table-bordered table-striped">
        <tr>
            <td>洗衣机</td>
            <td><a href="${pageContext.request.contextPath}/AddCartServlet?name=洗衣机">添加到购物车</a> </td>
        </tr>
        <tr>
            <td>电视机</td>
            <td><a href="${pageContext.request.contextPath}/AddCartServlet?name=电视机">添加到购物车</a> </td>
        </tr>
        <tr>
            <td>洗碗机</td>
            <td><a href="${pageContext.request.contextPath}/AddCartServlet?name=洗碗机">添加到购物车</a> </td>
        </tr>
        <tr>
            <td>打火机</td>
            <td><a href="${pageContext.request.contextPath}/AddCartServlet?name=打火机">添加到购物车</a> </td>
        </tr>
    </table>
    <h4><a href="${pageContext.request.contextPath}/Cart.jsp">查看购物车</a></h4>
</body>
</html>
